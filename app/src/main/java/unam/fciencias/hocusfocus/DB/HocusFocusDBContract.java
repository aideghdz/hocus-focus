package unam.fciencias.hocusfocus.DB;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contrato de la Base de datos.
 */

public class HocusFocusDBContract {
    //Como la aplicación es la dueña de la bd se emplea su ID como la autoridad.
    public static final String AUTHORITY = "unam.fciencias.hocusfocus";
    //Simplificador para la creación de URI's
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" +AUTHORITY);

    //Ocultamos el constructor para que nadie haga instancia de nuestro contrato.
    private HocusFocusDBContract(){

    }
    /**
     * Definimos toda la información necesaria para manejar y almacenar los hábitos.
     **/
    public static class HabitEntry implements BaseColumns {
        //Path a la información de los hábitos.
        public static final String PATH_HABIT = "habit";

        //URI para el contenido de esta colección
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_HABIT).build();

        //Nombre de la tabla.
        public static final String TABLE_NAME = PATH_HABIT;

        //Nombre de la columna donde se almacena el nombre de los hábitos.
        public static final String COLUMN_NAME = "name";

        //Nombre de la columna donde se almacenan los días de los hábitos.
        public static final String COLUMN_DAY = "day";

        //Nombre de la columna donde se almacena las horas de los hábitos.
        public static final String COLUMN_HOUR = "hour";

        //Nombre de la columna donde se almacena si esta activada la alarma de los hábitos.
        public static final String COLUMN_ALARM = "alarm";

        //Índice de la columna donde se almacena el ID
        public static final byte COLUMN_INDEX_ID = 0;

        //Índice de la columna donde se almacena el nombre
        public static final byte COLUMN_INDEX_NAME = 1;

        //Índice de la columna donde se almacena el día
        public static final byte COLUMN_INDEX_DAY = 2;

        //Índice de la columna donde se almacena la hora
        public static final byte COLUMN_INDEX_HOUR = 3;

        //Índice de la columna donde se almacena la alarma
        public static final byte COLUMN_INDEX_ALARM = 4;

        //Todas las tablas de las columnas
        public static final String[] COLUMNS = new String[] {_ID, COLUMN_NAME,COLUMN_DAY,COLUMN_HOUR,COLUMN_ALARM};

        //Tipo general de la colección
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +"/"
                +AUTHORITY +"/" +PATH_HABIT;

        //Tipo especifico de cada elemento de la colección.
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +"/"
                +AUTHORITY +"/" +PATH_HABIT;

        //Escondemos  el constructor para que no se generen instancias de la clase.
        private HabitEntry() {

        }

        //Simplificador para facilitar la construccion de una URI por el id buscado
        public static Uri buildListSizeUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Definimos toda la información necesaria para manejar y almacenar los sub hábitos.
     **/
    public static class SubHabitEntry implements BaseColumns {
        //Path para la información de los subhabitos.
        public static final String PATH_SUBHABIT = "subhabit";

        //URI para la colección
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SUBHABIT).build();

        //Nombre de la tabla
        public static final String TABLE_NAME = PATH_SUBHABIT;

        //Nombre de la columna donde almacenamos la id del hábito.
        public static final String COLUMN_HABIT = "habit";

        //Nombre de la columna donde almacenamos el nómbre del sub hábito
        public static final String COLUMN_NAME = "name";

        //Nombre de la columna donde almacenamos la duración del sub hábito
        public static final String COLUMN_LENGTH = "length";

        //Nombre de la columna donde almacenamos si se ha checado del sub hábito
        public static final String COLUMN_CHECKED = "checked";

        //Índice de la columna del ID.
        public static final int COLUMN_INDEX_ID = 0;

        //Índice de la columna del ID del hábito.
        public static final int COLUMN_INDEX_HABIT = 1;

        //Índice de la columna del nombre.
        public static final int COLUMN_INDEX_NAME = 2;

        //Índice de la columna de la duración.
        public static final int COLUMN_INDEX_LENGTH = 3;

        //Índice de la columna de la bandera de checado.
        public static final int COLUMN_INDEX_CHECKED = 4;

        //Todas las columnas de la tabla.
        public static final String[] COLUMNS = new String[] {_ID, COLUMN_HABIT,COLUMN_NAME,COLUMN_LENGTH, COLUMN_CHECKED};

        //Tipo general de la colección
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +"/"
                +AUTHORITY +"/" + PATH_SUBHABIT;

        //Tipo especifico de los elmentos individuales de la colección
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +"/"
                +AUTHORITY +"/" + PATH_SUBHABIT;

        //Escondemos el constructor.
        private SubHabitEntry() {

        }

        //Simplificador para facilitar la construccion de una URI por el id buscado
        public static Uri buildListSizeUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
    //Tabla no empleada donde se esperaba guardar la información de las notas.
    public static class NoteEntry implements BaseColumns{
        public static final String PATH_NOTE = "note";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOTE).build();

        public static final String TABLE_NAME = PATH_NOTE;

        public static final String COLUMN_TITLE = "title";

        public static final String COLUMN_CONTENTS = "contents";

        public static final String COLUMN_DATE = "date";

        public static final byte COLUMN_INDEX_ID = 0;

        public static final byte COLUMN_INDEX_TITLE = 1;

        public static final byte COLUMN_INDEX_CONTENTS = 2;

        public static final byte COLUMN_INDEX_DATE = 3;

        public static final String[] COLUMNS = new String[]{_ID, COLUMN_TITLE, COLUMN_CONTENTS, COLUMN_DATE};

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                AUTHORITY + "/" + PATH_NOTE;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                AUTHORITY + "/" + PATH_NOTE;

        private NoteEntry(){}

        public static Uri buildListSizeUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

}
