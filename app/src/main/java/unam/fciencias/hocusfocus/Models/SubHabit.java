package unam.fciencias.hocusfocus.Models;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;

import java.util.LinkedList;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;

public class SubHabit {
    private int id;
    private int habit;
    private String name;
    private int length;
    private boolean checked;

    public SubHabit(int id, int habit, String name, int length, boolean checked) {
        this.id = id;
        this.habit = habit;
        this.name = name;
        this.length = length;
        this.checked = checked;
    }

    public SubHabit(int habit, String name, int length, boolean checked) {
        this.id = 0;
        this.habit = habit;
        this.name = name;
        this.length = length;
        this.checked = checked;
    }

    public SubHabit() {

    }

    public int getId() {
        return id;
    }

    public int getHabit() {
        return habit;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHabit(int habit) {
        this.habit = habit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void update(String name, int length, boolean checked, Context context) {
        this.name = name;
        this.length = length;
        this.checked = checked;
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_NAME, name);
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_LENGTH, length);
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_CHECKED, checked);
        context.getContentResolver().update(
                ContentUris.withAppendedId((HocusFocusDBContract.SubHabitEntry.buildListSizeUri(this.id)), this.habit), values, null, null);
    }
    public String toString(){
        return "El nombre del Subhabito:"+ id +"La duracion "+length +"checkted"+checked;
    }

    public String getDuration(){
        return this.length +" min";
    }
    public void insert(Context context){
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT, habit);
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_NAME, name);
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_LENGTH, length);
        values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_CHECKED, checked);
        context.getContentResolver().insert(
                HocusFocusDBContract.SubHabitEntry.CONTENT_URI, values);
    }

    public void delete(Context context) {
        context.getContentResolver().delete(ContentUris.withAppendedId((HocusFocusDBContract.SubHabitEntry.buildListSizeUri(this.id)), this.habit), null, null);
    }

    public LinkedList<SubHabit> subHabitosPorHabito(int id_habito, Context context) {
        LinkedList<SubHabit> listContents = new LinkedList<SubHabit>();
        Resources res = context.getResources();
        Cursor cursor = (Cursor) context.getContentResolver().query(
                HocusFocusDBContract.SubHabitEntry.CONTENT_URI, new String[]{"*"}, null, null, null);
        boolean vacio = !cursor.moveToFirst();
        if (vacio) {
            return listContents;
        } else {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int index_id = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry._ID);
                int index_habit = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT);
                int index_name = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_NAME);
                int index_length = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_LENGTH);
                int index_check = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_CHECKED);
                SubHabit subHabit = new SubHabit(
                        cursor.getInt(index_id), cursor.getInt(index_habit),
                        cursor.getString(index_name), cursor.getInt(index_length),
                        Boolean.parseBoolean(cursor.getString(index_check)));
                if (subHabit.getHabit() == id_habito)
                    listContents.add(subHabit);
            }
            return listContents;
        }

    }
}
