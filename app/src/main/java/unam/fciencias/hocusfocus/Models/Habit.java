package unam.fciencias.hocusfocus.Models;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.Calendar;
import java.util.LinkedList;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;

public class Habit {
    private  int id;
    private  String name;
    private  int day;
    private  int hour;
    private  boolean alarm;
    private static final String CERO = "0";
    public Habit (int id, String name, int day, int hour, boolean alarm){
        this.id = id;
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.alarm = alarm;
    }

    public Habit (String name, int day, int hour, boolean alarm){
        this.id = Integer.parseInt(null);
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.alarm = alarm;
    }

    public Habit() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public boolean getAlarm() {
        return alarm;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public void update(String name, int day, int hour, boolean alarm, Context context){
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.alarm = alarm;
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_NAME,name);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_DAY, day);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_HOUR,hour);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_ALARM,alarm);
        context.getContentResolver().update(
                HocusFocusDBContract.HabitEntry.buildListSizeUri(this.id),values,null,null);
    }
    public Uri insert(Context context){
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_NAME,name);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_DAY, day);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_HOUR,hour);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_ALARM,alarm);
        return context.getContentResolver().insert(
                HocusFocusDBContract.HabitEntry.CONTENT_URI,values);
    }

    public void delete(Context context){
        Calendar c = Calendar.getInstance();
        int tiempo = this.hour;
        int min = tiempo%100;
        int horas = (tiempo-min)/100;
        for (Integer i : getDays()) {
            c.set(Calendar.DAY_OF_WEEK, i);
            c.set(Calendar.HOUR_OF_DAY, horas);
            c.set(Calendar.MINUTE, min);
            long t = c.getTimeInMillis();
            int notificationId = (i * 1000) + id;
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent("HocusFocus.action.DISPLAY_NOTIFICATION");
            intent.putExtra("ID", notificationId);
            intent.putExtra("Nombre", this.name);
            intent.putExtra("Hora", t);
            PendingIntent broadcast = PendingIntent.getBroadcast(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(broadcast);
        }
        context.getContentResolver().delete(HocusFocusDBContract.HabitEntry.buildListSizeUri(this.id), null, null);
    }
    public LinkedList<Integer> getDays(){
        LinkedList<Integer> dias =  new LinkedList<Integer>();
        int dia = this.day;
        if(dia-1000000 >= 0){
            dias.add(1);
            dia-=1000000;
        }
        if(dia-100000 >= 0){
            dias.add(7);
            dia-=100000;
        }
        if(dia-10000>= 0){
            dias.add(6);
            dia-= 10000;
        }
        if(dia-1000>= 0){
            dias.add(5);
            dia-= 1000;
        }
        if(dia-100>= 0){
            dias.add(4);
            dia-=100;
        }
        if(dia-10>= 0){
            dias.add(3);
            dia-=10;
        }
        if(dia-1>= 0){
            dias.add(2);
            dia-=1;
        }
        return dias;
    }

    public String hourToSting(){
        String s = "";
        int min = hour%100;
        int horas = (hour-min)/100;
        String horaFormateada =  (horas < 10)? String.valueOf(CERO + horas) : String.valueOf(horas);
        //Formateo el minuto obtenido: antepone el 0 si son menores de 10
        String minutoFormateado = (min < 10)? String.valueOf(CERO + min):String.valueOf(min);
        if(hour< 12) {
            s+= "a.m.";
        } else {
            s+= "p.m.";
        }
        return horaFormateada + ":" + minutoFormateado + " " + s;

    }

    @Override
    public String toString(){

        String s = "";
        int min = hour%100;
        int horas = (hour-min)/100;
        if(min <10){
            s = Integer.toString(horas) + ":"+"0"+Integer.toString(min);
        }else {
            s = Integer.toString(horas) + ":" + Integer.toString(min);
        }
        return id+") "+ s+" "+name;
    }

    public void update( Context context){
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_NAME,this.name);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_DAY, this.day);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_HOUR,this.hour);
        values.put(HocusFocusDBContract.HabitEntry.COLUMN_ALARM,this.alarm);
        context.getContentResolver().update(
                HocusFocusDBContract.HabitEntry.buildListSizeUri(this.id),values,null,null);
    }
}
