package unam.fciencias.hocusfocus.Loaders;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.Models.Habit;

public class HabitListLoader extends AsyncTaskLoader<List<Habit>> {

    private List<Habit> listContents;

    public static final byte MASTER_LIST_LOADER_ID = 4;

    public boolean vacio = false;

    public HabitListLoader(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public List<Habit> loadInBackground() throws IllegalStateException {
        listContents = new LinkedList<Habit>();
        Resources res = getContext().getResources();
        Cursor cursor = getLowerHabit();
        vacio = !cursor.moveToFirst();
        if(vacio){
            return listContents;
        }else {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
                int index_id = cursor.getColumnIndex(HocusFocusDBContract.HabitEntry._ID);
                int index_name = cursor.getColumnIndex(HocusFocusDBContract.HabitEntry.COLUMN_NAME);
                int index_day = cursor.getColumnIndex(HocusFocusDBContract.HabitEntry.COLUMN_DAY);
                int index_hour = cursor.getColumnIndex(HocusFocusDBContract.HabitEntry.COLUMN_HOUR);
                int index_alarm = cursor.getColumnIndex(HocusFocusDBContract.HabitEntry.COLUMN_ALARM);
                Habit habit = new Habit(
                    cursor.getInt(index_id), cursor.getString(index_name),
                    cursor.getInt(index_day), cursor.getInt(index_hour),
                    Boolean.parseBoolean(cursor.getString(index_alarm)));
                listContents.add(habit);
            }
            return listContents;
        }
    }

    public Cursor getLowerHabit() {
        Cursor cursor = (Cursor) this.getContext().getContentResolver().query(
                HocusFocusDBContract.HabitEntry.CONTENT_URI, new String[]{"*"}, null, null, null);
        return cursor;
    }


    public static Uri buildListSizeEntryUri(long i) {
        return HocusFocusDBContract.HabitEntry.buildListSizeUri(i);
    }


}
