package unam.fciencias.hocusfocus;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.Adapters.SubhabitoListAdapter;
import unam.fciencias.hocusfocus.Loaders.SubHabitLoader;
import unam.fciencias.hocusfocus.Models.Habit;
import unam.fciencias.hocusfocus.Models.SubHabit;

/**
 * Actividad que permite crear un nuevo Hábito.
 */
public class NewHabito extends AppCompatActivity implements View.OnClickListener{
    //atributos

    /*Checkbox for the days*/
    private CheckBox LUNES,MARTES,MIERCOLES,JUEVES,VIERNES,SABADO,DOMINGO;

    /*Array to have all the CheckBox*/
    private CheckBox[] DAYS;

    /*The view text for the habit name*/
    private  EditText habit_name;

    /*The text view for hour*/
    private EditText habit_hour_text;

    //Constante de milisegundos en una semana
    private final int miliEnSemana = 604800000;

    /*Habits to make the uptades or insertions*/
    public static Habit HABITO;

    /*Bolena to know what method call if update or insert*/
    public static boolean update ;

    private static final String CERO = "0";

    private static final String DOS_PUNTOS = ":";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    //Recursos de las vistas
    EditText etHora;
    FloatingActionButton ibObtenerHora;
    private TextView textView;
    private RecyclerView rv_subhabits;
    private SubhabitoListAdapter llm_subhabits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_habit);
        //Oteniendo los checkboxed para los dias
        LUNES = (CheckBox) findViewById(R.id.lunes_button);
        MARTES = (CheckBox) findViewById(R.id.martes_button);
        MIERCOLES = (CheckBox) findViewById(R.id.miercoles_button);
        JUEVES = (CheckBox) findViewById(R.id.jueves_button);
        VIERNES = (CheckBox) findViewById(R.id.viernes_button);
        SABADO = (CheckBox) findViewById(R.id.sabado_button);
        DOMINGO = (CheckBox) findViewById(R.id.domingo_button);
        DAYS  = new CheckBox[]{LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO};

        //Widget EditText donde se mostrara la hora obtenida
        etHora = (EditText) findViewById(R.id.habito_hora_text);
        //Widget ImageButton del cual usaremos el evento clic para obtener la hora
        ibObtenerHora =  findViewById(R.id.habito_hora);
        //Evento setOnClickListener - clic
        ibObtenerHora.setOnClickListener(this);
        etHora.setOnClickListener(this);

        textView = findViewById(R.id.subhabit_duration);
        rv_subhabits = findViewById(R.id.rv_subhabits);
        rv_subhabits.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_subhabits.setLayoutManager(layoutManager);

        llm_subhabits = new SubhabitoListAdapter(getResources(),getSupportFragmentManager());
        rv_subhabits.setAdapter(llm_subhabits);
        rv_subhabits.setItemAnimator(new DefaultItemAnimator());

        //All the fields of the habit
        habit_name = findViewById(R.id.nuevo_habito_nombre);
        habit_hour_text = findViewById(R.id.habito_hora_text);
        //Saber si es un nuevo hábito o si se editará alguno
        if(HABITO == null){
            HABITO = new Habit();

        }else{
            fillViewFields();
        }
    }

    /**
     * Método que rellena los datos de un hábito que se está editando
     */
    private void fillViewFields() {
        habit_name.setText(HABITO.getName());
        String hour = "";
        habit_hour_text.setText(HABITO.hourToSting());
        setDays();
        llm_subhabits.setDataSet(loadSubhabits()); //carga los subhábitos
    }

    /**
     * Método que obtiene de la BD los subhábitos del hábito que se está editando.
     */
    private  List<SubHabit> loadSubhabits(){
        SubHabitLoader subLoader = new SubHabitLoader(this);
        List<SubHabit> subHabits = subLoader.loadInBackground();
        LinkedList<SubHabit> result = new LinkedList<>();
        for(SubHabit sh : subHabits){
            if(sh.getHabit() == HABITO.getId()){
                result.add(sh);
            }

        }
        return result;
    }

    /**
     * Método que marca los días de un hábito que está por editarse.
     */
    private void setDays() {
        LinkedList<Integer> days =getDays();
        for(Integer i : days){
            if(i!=1){
                DAYS[i-2].setChecked(true);
            }else{
                DAYS[6].setChecked(true);
            }


        }
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.habito_hora:
                obtenerHora();
                break;
            case R.id.habito_hora_text:
                obtenerHora();
                break;
        }
    }

    /**
     * Método que obtiene la hora del timepicker.
     */
    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                etHora.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    /**
     * Método que cierra el fragmento y limpia las variables.
     */
    public void closeHabit(View view){
        HABITO= null;
        update = false;
        finish();
    }

    /**
     * Método que establece la codificación a guardar en la BD de los días de la semana.
     */
    private int sumDays(){
        int sum = 0 ;
        if(LUNES.isChecked()){
            sum =+1;
        }
        if(MARTES.isChecked()){
            sum+=10;

        }

        if(MIERCOLES.isChecked()){
            sum+=100;

        }
        if(JUEVES.isChecked()){
            sum+=1000;

        }
        if(VIERNES.isChecked()){
            sum+=10000;

        }
        if(SABADO.isChecked()){
            sum+=100000;

        }
        if(DOMINGO.isChecked()){
            sum+=1000000;

        }
        return sum;
    }


    /**
     * Método que agrega el hábito a la BD.
     */
    public void  addHabit(View view){
        EditText txtname = habit_name;
        String nombre_habito = txtname.getText().toString();
        //validaciones
        if(nombre_habito.equalsIgnoreCase(""))
        {
            txtname.setError("Ingresar nombre para el hábito");
            txtname.hasFocus();
            return;
        }
        String hora = etHora.getText().toString();
        if(hora.equals("")){
            habit_hour_text.setError("Definir una hora");
            habit_hour_text.hasFocus();
            return;
        }
        hora = hora.substring(0,2)+ hora.substring(3,5);
        HABITO.setName(nombre_habito);
        HABITO.setDay(sumDays());
        HABITO.setAlarm(true);
        HABITO.setHour(Integer.parseInt(hora));
        int id;
        if(update){
            id = HABITO.getId();
            HABITO.update(this);
        }else{
            Uri uri = HABITO.insert(this);
            id = Integer.parseInt(uri.getPathSegments().get(1));
        }

        HABITO.setId(id);
        int tiempo = HABITO.getHour();
        int min = tiempo%100;
        int horas = (tiempo-min)/100;
        for (Integer i : getDays()) {
            c.set(Calendar.DAY_OF_WEEK, i);
            c.set(Calendar.HOUR_OF_DAY, horas);
            c.set(Calendar.MINUTE, min);
            scheduleNotification(this, c.getTimeInMillis(), (i*1000)+id, HABITO.getName());
        }
        //Inserción de los subhabitos
        List<SubHabit> subHabitsOld = llm_subhabits.getDataSet();
        LinkedList<SubHabit> aux = new LinkedList<>();
        for(SubHabit sh : subHabitsOld){
            sh.delete(this);
        }
        for(int i = 0; i < llm_subhabits.getItemCount(); i++){
            SubhabitoListAdapter.SubhabitoVH shv =
                    (SubhabitoListAdapter.SubhabitoVH) rv_subhabits.findViewHolderForAdapterPosition(i);
            int duracion = shv.getDuration();
            String name = shv.getDescripcion_subhabito().getText().toString();
            SubHabit subHabit = new SubHabit(id,name,duracion,false);
            subHabit.insert(this);
        }

        HABITO = null;
        update=false;
        finish();
    }

    /**
     * Método que obtiene los días de la semana de un habito extraídos de la BD,
     * realizándolo con una descodificación.
     */
    public LinkedList<Integer> getDays(){
        LinkedList<Integer> dias =  new LinkedList<Integer>();
        int dia = HABITO.getDay();
        if(dia-1000000 >= 0){
            dias.add(1);
            dia-=1000000;
        }
        if(dia-100000 >= 0){
            dias.add(7);
            dia-=100000;
        }
        if(dia-10000>= 0){
            dias.add(6);
            dia-= 10000;
        }
        if(dia-1000>= 0){
            dias.add(5);
            dia-= 1000;
        }
        if(dia-100>= 0){
            dias.add(4);
            dia-=100;
        }
        if(dia-10>= 0){
            dias.add(3);
            dia-=10;
        }
        if(dia-1>= 0){
            dias.add(2);
            dia-=1;
        }
        return dias;
    }

    /**
     * Método que establece la notificación de una hábito para la hora y día indicados,
     * una vez que se guarda el hábito.
     */
    public static void scheduleNotification( Context context, long time, int notificationId, String name ) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent("HocusFocus.action.DISPLAY_NOTIFICATION");
        intent.putExtra("ID", notificationId);
        intent.putExtra("Nombre", name);
        intent.putExtra("Hora", time);
        PendingIntent broadcast = PendingIntent.getBroadcast(context,notificationId,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, broadcast);

    }

    /**
     * Método que añade un subhabito a la lista de subhabitos
     */
    public void addSubhabit(View view){
        SubHabit sh = new SubHabit(HABITO.getId(),"",0,false);
        sh.insert(this);
        llm_subhabits.addItem(sh);
    }

}