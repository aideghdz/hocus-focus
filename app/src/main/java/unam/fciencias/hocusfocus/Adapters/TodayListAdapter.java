package unam.fciencias.hocusfocus.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class TodayListAdapter extends RecyclerView.Adapter<TodayListAdapter.TodayEntry> {

    @NonNull
    @Override
    public TodayEntry onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull TodayEntry todayEntry, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class TodayEntry extends RecyclerView.ViewHolder {
        public TodayEntry(@NonNull View itemView) {
            super(itemView);
        }
    }
}
