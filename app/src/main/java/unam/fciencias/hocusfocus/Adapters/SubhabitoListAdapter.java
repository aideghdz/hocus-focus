package unam.fciencias.hocusfocus.Adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.Models.SubHabit;
import unam.fciencias.hocusfocus.NumberPickerDialog;
import unam.fciencias.hocusfocus.R;

public class SubhabitoListAdapter  extends RecyclerView.Adapter<SubhabitoListAdapter.SubhabitoVH>{

    private List<SubHabit> dataset;
    private Resources resources;
    private FragmentManager fragmentManager;
    private int size;

    public void setDataSet(List<SubHabit> subHabits) {
        dataset = subHabits;
    }

    public List<SubHabit> getDataSet() {
        return dataset;
    }


    public class SubhabitoVH extends RecyclerView.ViewHolder implements NumberPicker.OnValueChangeListener {
        private CardView cv;
        private TextView duracion_subhabito;
        private TextView descripcion_subhabito;
        private FloatingActionButton delete_subhabito;
        private NumberPicker np;
        private int duration;

        private final int[] duraciones = {1,2,3,5,10,15,30};

        SubhabitoVH(CardView cv) {
            super(cv);
            cv.setFocusable(true);
            cv.setClickable(true);
            duracion_subhabito = cv.findViewById(R.id.subhabit_duration);
            descripcion_subhabito = cv.findViewById(R.id.subhabit_description);
            delete_subhabito = cv.findViewById(R.id.delete_subhabit);
            duracion_subhabito.setFocusable(true);
            duracion_subhabito.setClickable(true);
            duration = 0;
            duracion_subhabito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showNumberPicker(view);
                }
            });
            delete_subhabito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    SubHabit subhabit = dataset.remove(position);
                    subhabit.delete(view.getContext());
                    notifyItemRemoved(getAdapterPosition());
                }
            });
        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            duracion_subhabito.setText(resources.getStringArray(R.array.valores_picker_subhabitos)[picker.getValue()]);
            duration = duraciones[picker.getValue()];
        }

        public void showNumberPicker(View view){
            NumberPickerDialog newFragment = new NumberPickerDialog();
            newFragment.setValueChangeListener(this);
            newFragment.show(fragmentManager, "time picker");
        }

        public CardView getCv() {
            return cv;
        }

        public TextView getDuracion_subhabito() {
            return duracion_subhabito;
        }

        public TextView getDescripcion_subhabito() {
            return descripcion_subhabito;
        }

        public FloatingActionButton getDelete_subhabito() {
            return delete_subhabito;
        }

        public NumberPicker getNp() {
            return np;
        }

        public int getDuration() {
            duration = Integer.valueOf(duracion_subhabito.getText().toString().replace(" min", ""));
            return duration;
        }

    }

    public SubhabitoListAdapter(Resources resources, FragmentManager fragmentManager){
        this.resources = resources;
        this.fragmentManager = fragmentManager;
        this.dataset = new LinkedList<>();

    }

    public void addItem(SubHabit sh){
        dataset.add(sh);
        notifyItemInserted(dataset.size()-1);
    }

    @NonNull
    @Override
    public SubhabitoListAdapter.SubhabitoVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView tv = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.new_subhabit_entry, viewGroup, false);
        return new SubhabitoVH(tv);

    }

    @Override
    public void onBindViewHolder(@NonNull SubhabitoListAdapter.SubhabitoVH subhabitoVH, int i) {
        subhabitoVH.duracion_subhabito.setText(dataset.get(i).getDuration());
        subhabitoVH.descripcion_subhabito.setText(dataset.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return dataset != null ? dataset.size() : 0;
    }

}