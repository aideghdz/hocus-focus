package unam.fciencias.hocusfocus;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import unam.fciencias.hocusfocus.Adapters.HabitListAdapter;
import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.DB.HocusFocusDbHelper;
import unam.fciencias.hocusfocus.Loaders.HabitListLoader;
import unam.fciencias.hocusfocus.Models.Habit;

/**
 * Clase sin uso. Era un primer prototipo de cada una de las pestañas de los días
 */
public class HabitosListadoView extends Fragment
        implements HabitListAdapter.MasterListItemClickHandler,
        LoaderManager.LoaderCallbacks{

    private HabitListAdapter listAdapter;

    private boolean hasListChanged;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_habitos_listado_view, container, false);
        RecyclerView habitList = view.findViewById(R.id.lista_infinita);
        habitList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        habitList.setLayoutManager(layoutManager);
        listAdapter = new HabitListAdapter(getResources(), this);
        habitList.setAdapter(listAdapter);
        LoaderManager.getInstance(this).initLoader(HocusFocusDbHelper.LIST_LOADER_ID,
                null, this);
        return view;
    }


    @Override
    public void onStop() {
        super.onStop();
    }//onDestroy

    @Override
    public void onItemClicked(String entryText) {

    }//onItemClicked

    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        switch (i) {
            case HabitListLoader.MASTER_LIST_LOADER_ID :
                return new HabitListLoader(this.getContext());

            case HocusFocusDbHelper.LIST_LOADER_ID :
                return new CursorLoader(this.getContext(), HabitListLoader.buildListSizeEntryUri(0),
                        new String[] {HocusFocusDBContract.HabitEntry._ID},
                        null, null, null);
            default :
                throw new UnsupportedOperationException("Can't create a Loader with ID " + i);
        }//creates the appropriate loader
    }//onCreateLoader

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object results) {
        if(results instanceof Cursor) {
            Cursor result = (Cursor) results;
            Bundle listLoaderParams = new Bundle();
            listLoaderParams = null;
            LoaderManager.getInstance(this).initLoader(HabitListLoader.MASTER_LIST_LOADER_ID,
                    listLoaderParams, this).forceLoad();
        } else {
            listAdapter.setDataset((List<Habit>) results);
        }//reacts to the finalization of a Loader
    }//onLoadFinished

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        // unused
    }//onLoaderReset
}
