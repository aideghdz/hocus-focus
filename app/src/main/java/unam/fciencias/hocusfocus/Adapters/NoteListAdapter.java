package unam.fciencias.hocusfocus.Adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import unam.fciencias.hocusfocus.Models.Note;
import unam.fciencias.hocusfocus.R;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteEntry> {
    private List<Note> dataset;

    private final Resources RESOURCES;

    private final NoteListAdapter.MasterListItemClickHandler CLICK_HANDLER;

    public NoteListAdapter(Resources res,
                           NoteListAdapter.MasterListItemClickHandler listItemClickHandler) {
        RESOURCES = res;
        CLICK_HANDLER = listItemClickHandler;
    }

    @NonNull
    @Override
    public NoteEntry onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View tv = (View) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.nota_single_view, viewGroup, false);
        return new NoteEntry(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteEntry noteEntry, int i) {
        noteEntry.noteTitle.setText(dataset.get(i).getTitle());
        noteEntry.noteDate.setText(dataset.get(i).formattedDateNoteRecyclerView());
    }

    @Override
    public int getItemCount() {
        return dataset != null ? dataset.size() : 0;
    }

    public void setDataset(List<Note> dataset) {
        this.dataset = dataset;
        notifyDataSetChanged();
    }

    class NoteEntry extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView noteTitle;
        private TextView noteDate;
        private FloatingActionButton deleteButton;

        NoteEntry(View entry){
            super(entry);
            noteTitle = (TextView) entry.findViewById(R.id.note_title_tv);
            noteDate = (TextView) entry.findViewById(R.id.note_date_tv);
            deleteButton = (FloatingActionButton) entry.findViewById(R.id.delete_note_b);
            deleteButton.setOnClickListener(new Button.OnClickListener(){
                public void onClick(View v){
                    int position = getAdapterPosition();
                    Note note = dataset.remove(position);
                    note.delete(v.getContext());
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, dataset.size());
                }
            });
        }

        @Override
        public void onClick(View v) {
            CLICK_HANDLER.onItemClicked(noteTitle.getText().toString());
        }
    }

    public interface MasterListItemClickHandler {
        void onItemClicked(String entryText);
    }

}
