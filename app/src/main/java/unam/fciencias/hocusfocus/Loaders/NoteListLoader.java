package unam.fciencias.hocusfocus.Loaders;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.Models.Note;

public class NoteListLoader extends AsyncTaskLoader<List<Note>> {
    private List<Note> listContents;

    public static final byte MASTER_LIST_LOADER_ID = 7;

    public boolean vacio = false;

    public boolean prueba = false;

    public NoteListLoader(@NonNull Context context){
        super(context);
    }

    @Nullable
    @Override
    public List<Note> loadInBackground() {
        listContents = new LinkedList<>();
        Resources res = getContext().getResources();
        Cursor cursor = getLowerNote();
        vacio = !cursor.moveToFirst();
        if(vacio) {
            return listContents;
        }
        else {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int indexId = cursor.getColumnIndex(HocusFocusDBContract.NoteEntry._ID);
                int indexTitle = cursor.getColumnIndex(HocusFocusDBContract.NoteEntry.COLUMN_TITLE);
                int indextContents =
                        cursor.getColumnIndex(HocusFocusDBContract.NoteEntry.COLUMN_CONTENTS);
                int indexDate = cursor.getColumnIndex(HocusFocusDBContract.NoteEntry.COLUMN_DATE);
                Note note = new Note(
                        cursor.getInt(indexId), cursor.getString(indexTitle),
                        cursor.getString(indextContents), cursor.getLong(indexDate)
                );
                listContents.add(note);
            }
        }

        return listContents;
    }

    public Cursor getLowerNote() {
        Cursor cursor = (Cursor) this.getContext().getContentResolver().query(
                HocusFocusDBContract.NoteEntry.CONTENT_URI, new String[]{"*"},
                null, null, null);
        return cursor;
    }

    public static Uri buildListSizeEntryUri(long i) {
        return HocusFocusDBContract.NoteEntry.buildListSizeUri(i);
    }
}
