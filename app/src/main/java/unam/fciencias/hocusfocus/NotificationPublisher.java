package unam.fciencias.hocusfocus;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;

import java.util.Calendar;

//Notificación de la aplicación.
public class NotificationPublisher extends BroadcastReceiver {
    //Método que lanza la notificación y agenda la siguiente.
    @Override
    public void onReceive(final Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, SplashActivity.class);

        long hora = intent.getLongExtra("Hora",0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(hora);
        long repetir = calendar.getTimeInMillis() + 604800000;
        int id = intent.getIntExtra("ID",0);
        String nombre = intent.getStringExtra("Nombre");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntentWithParentStack(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(id ,PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context);

        Notification notification = builder.setContentTitle(nombre)
                .setContentText("Es momento de:"+nombre)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.clock)
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hocus_focus)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent).build();


        NewHabito.scheduleNotification(context, repetir, id, nombre);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
    }

}