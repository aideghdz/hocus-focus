package unam.fciencias.hocusfocus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.tomerrosenfeld.customanalogclockview.CustomAnalogClock;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Actividad principal, ie, HOME. Primera actividad mostrada al usuario después del splash.
 * Permite al usuario navegar a las pantallas de Habitos, Hoy y Notas.
 */
public class MainActivity extends AppCompatActivity {

    //atributos
    /*The date pattern*/
    private  static  String DATE_PATTERN= "dd  MMMM yyy";

    /*The scale of the clock */
    private  static float CLOCK_SCALE = 0.7f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomAnalogClock customAnalogClock = (CustomAnalogClock) findViewById(R.id.analog_clock); //Reloj mostrado en el botón de Hoy
        customAnalogClock.setAutoUpdate(true);
        customAnalogClock.setScale(CLOCK_SCALE);
        String date_n =getText( R.string.today)
                +new SimpleDateFormat(DATE_PATTERN, Locale.getDefault()).format(new Date());
        TextView date  = (TextView) findViewById(R.id.today_view_text);
        date.setText(date_n);


    }

    /*For show the about dialog for the app*/
    public void showAboutDialog(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.menu_about)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(R.string.about)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }//onClick
                })
                .create().show();
    }

    /**
     * Método que redirige a la actividad de Hoy a través de un intent.
     */
    public void opentTodayTask(View view) {
        MenuController.option = getString(R.string.hoy);
        Intent myIntent = new Intent(getBaseContext(), MenuController.class);
        startActivity(myIntent);
    }

    /**
     * Método que redirige a la actividad de Hábitos a través de un intent.
     */
    public void openHabits(View view) {
        MenuController.option = getString(R.string.habitos);
        Intent myIntent = new Intent(getBaseContext(), MenuController.class);
        startActivity(myIntent);
    }

    /**
     * Método que redirige a la actividad de Notas a través de un intent.
     */
    public void openNotes(View view) {
        MenuController.option = getString(R.string.notas);
        Intent myIntent = new Intent(getBaseContext(), MenuController.class);
        startActivity(myIntent);
    }




}
