package unam.fciencias.hocusfocus.Loaders;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.Models.SubHabit;

public class SubHabitLoader extends AsyncTaskLoader<List<SubHabit>>{
    private List<SubHabit> listContents;

    public static final byte MASTER_LIST_LOADER_ID = 4;

    public boolean vacio = false;

    public SubHabitLoader(@NonNull Context context){ super(context); }

    @Nullable
    @Override
    public List<SubHabit> loadInBackground() {
        listContents = new LinkedList<SubHabit>();
        Resources res = getContext().getResources();
        Cursor cursor = getSubHabit();
        vacio = !cursor.moveToFirst();
        if(vacio){
            return listContents;
        }else {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
                int index_id = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry._ID);
                int index_habit =cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT);
                int index_name = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_NAME);
                int index_length =cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_LENGTH);
                int index_check = cursor.getColumnIndex(HocusFocusDBContract.SubHabitEntry.COLUMN_CHECKED);
                SubHabit subHabit = new SubHabit(
                        cursor.getInt(index_id)
                        ,cursor.getInt(index_habit),
                        cursor.getString(index_name), cursor.getInt(index_length),
                        Boolean.parseBoolean(cursor.getString(index_check)));
                listContents.add(subHabit);
                System.out.println("El subhabito creado"+ subHabit.toString());
            }
            int lon = listContents.size();
            return listContents;
        }

    }
    public Cursor getSubHabit() {
        Cursor cursor = (Cursor) this.getContext().getContentResolver().query(
                HocusFocusDBContract.SubHabitEntry.CONTENT_URI, null, null, null, null);
        return cursor;
    }

}
