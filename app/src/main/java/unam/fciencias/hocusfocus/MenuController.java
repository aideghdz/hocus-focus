package unam.fciencias.hocusfocus;

import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

/**
 * Clase que implementa el Menú de hamburguesa.
 */
public class MenuController extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener{
    //atributos

    /**
     * Referencia a la barra superior de la pantalla, donde cambiaremos el nombre
     * dependiendo de a donde se dirija.
     */
    private Toolbar toolbar;

    public static String option = "Home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        changeContent(option);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        String menu = (String) menuItem.getTitle();
        changeContent(menu);
        return true;
    }

    /**
     * Método que cambia el contenido del fragmento por el solicitado en el menú.
     */
    private  void changeContent(String menu){
        FragmentManager fm = getSupportFragmentManager();
        String title = "";
        switch (menu){
            case "Home":
                Intent homeIntent
                        = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(homeIntent);
                break;
            case "Hoy":
                fm.beginTransaction().replace(R.id.main_content,new HoyListadoView()).commit();
                break;
            case "Hábitos":
                fm.beginTransaction().replace(R.id.main_content,new Habitos()).commit();
                break;
            case "Notas":
                fm.beginTransaction().replace(R.id.main_content,new NotasListadoView()).commit();
                break;
            case "Configuración":
                fm.beginTransaction().replace(R.id.main_content,new ConfigurationView()).commit();
                break;

        }
        toolbar.setTitle(menu);
        toolbar.setTitleMarginStart(40);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void openDetailViewHabit(View view){
        Intent myIntent = new Intent(this,NewHabito.class);
        startActivity(myIntent);
    }

    public void openDetailViewNewNote(View view){
        Intent myIntent = new Intent(this, NewNote.class);
        startActivity(myIntent);
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    /**
     * Método que permite cambiar el sonido de la alarma de una hábito.
     */
    public void chooseHabitSound(View arg0) {
        final Uri currentTone= RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM);
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        startActivityForResult(intent, 5);
    }
    @Override
    protected void onResume() {
        super.onResume();
        changeContent(option);
    }
}
