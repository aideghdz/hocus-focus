package unam.fciencias.hocusfocus;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Administrador de las pestañas de los días de la semana en hábitos
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    //Número de pestañas que agregaremos
    private int numOfTabs;

    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    /**
     * Método que selecciona el framento dependiendo del valor seleccionado.
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                LunesView lunes = new LunesView();
                return lunes;
            case 1:
                MartesView martes = new MartesView();
                return martes;
            case 2:
                MiercolesView miercoles = new MiercolesView();
                return miercoles;
            case 3:
                JuevesView jueves = new JuevesView();
                return jueves;
            case 4:
                ViernesView viernes = new ViernesView();
                return viernes;
            case 5:
                SabadoView sabado = new SabadoView();
                return sabado;
            case  6:
                DomingoView domingo = new DomingoView();
                return domingo;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
