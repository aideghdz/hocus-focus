package unam.fciencias.hocusfocus.DB;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.DB.HocusFocusDbHelper;


public class HocusFocusProvider extends ContentProvider {
    // attibutes

    /**
     * Helper to communicate with the system's SQLite engine.
     * @since List Provider 1.0, april 2019
     */
    private HocusFocusDbHelper dbHelper;

    /**
     * Represents the content type <i>habit</i>
     */
    public static final byte HABIT = 100;

    /**
     * Represents the CRUD query for <i>habit</i> entries using an ID.
     */
    public static final byte HABIT_WITH_ID = 101;
    /**
    * Represents the content type <i>subhabit</i>
    */
    public static final int SUBHABIT = 200;

    /**
     * Represents the CRUD query for <i>subhabit</i>
     */

    public static final int SUBHABIT_WITH_ID = 201;

    /**
     * Represents the CRUD query for <i>note</i>
     */

    public static final int NOTE = 300;

    /**
     *  Represents the CRUD query for <i>note</i> entries using an ID.
     */

    public static final int NOTE_WITH_ID = 301;

    /**
    * UriMatcher to match a URI in a CRUD query, so the ListProvider can perform the
     * adequate query on the collection indicated by the PATH in the URI.
     */
    private static final UriMatcher CONTENT_MATCHER;

    // class initializer
    static {
        CONTENT_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.HabitEntry.PATH_HABIT, HABIT);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.HabitEntry.PATH_HABIT +"/#",
                HABIT_WITH_ID);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.SubHabitEntry.PATH_SUBHABIT,SUBHABIT);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.SubHabitEntry.PATH_SUBHABIT+"/#"+"/#", SUBHABIT_WITH_ID);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.NoteEntry.PATH_NOTE, NOTE);
        CONTENT_MATCHER.addURI(HocusFocusDBContract.AUTHORITY,
                HocusFocusDBContract.NoteEntry.PATH_NOTE + "/#", NOTE_WITH_ID);
    }//class initializer

    // methods

    @Override
    public boolean onCreate() {
        dbHelper = new HocusFocusDbHelper(getContext());
        return true;
    }//onCreate

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor result;
        switch (CONTENT_MATCHER.match(uri)) {
            case HABIT :
                result = db.query(HocusFocusDBContract.HabitEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case HABIT_WITH_ID :
                result = db.query(HocusFocusDBContract.HabitEntry.TABLE_NAME, projection,
                        HocusFocusDBContract.HabitEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)}, null, null,
                        null);
                break;
            case SUBHABIT:
                result = db.query(HocusFocusDBContract.SubHabitEntry.TABLE_NAME,projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case SUBHABIT_WITH_ID:
                result = db.query(HocusFocusDBContract.SubHabitEntry.TABLE_NAME, projection,
                        HocusFocusDBContract.SubHabitEntry._ID +
                                " = ? AND "+HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT
                                + " = ? ",
                        new String[] {uri.getPathSegments().get(1),
                                uri.getPathSegments().get(2)},null, null,
                        null);
            case NOTE:
                result = db.query(HocusFocusDBContract.NoteEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null,null, sortOrder);
                break;
            case NOTE_WITH_ID:
                result = db.query(HocusFocusDBContract.NoteEntry.TABLE_NAME, projection,
                        HocusFocusDBContract.NoteEntry._ID + " = ?",
                        new String[] {uri.getPathSegments().get(1)}, null, null,
                        null);
                break;
            default :
                throw new UnsupportedOperationException("Invalid URI: " +uri);
        }//performs a query depending on the given URI
        Context context = getContext();
        if(context != null) result.setNotificationUri(context.getContentResolver(), uri);
        return result;
    }//query


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newId;
        switch (CONTENT_MATCHER.match(uri)) {
            case HABIT :
                newId = db.insert(HocusFocusDBContract.HabitEntry.TABLE_NAME, null, values);
                break;
            case HABIT_WITH_ID :
                if(values == null) values = new ContentValues();
                values.put(HocusFocusDBContract.HabitEntry._ID, uri.getPathSegments().get(1));
                newId = db.insert(HocusFocusDBContract.HabitEntry.TABLE_NAME, null, values);
                break;
            case SUBHABIT :
                newId = db.insert(HocusFocusDBContract.SubHabitEntry.TABLE_NAME, null, values);
                break;
            case SUBHABIT_WITH_ID:
                if(values == null) values = new ContentValues();
                values.put(HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT, uri.getPathSegments().get(1));
                values.put(HocusFocusDBContract.SubHabitEntry._ID, uri.getPathSegments().get(2));
                newId = db.insert(HocusFocusDBContract.SubHabitEntry.TABLE_NAME, null, values);
                break;
            case NOTE:
                newId = db.insert(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        null, values);
                break;
            case NOTE_WITH_ID:
                if(values == null) values = new ContentValues();
                values.put(HocusFocusDBContract.NoteEntry._ID, uri.getPathSegments().get(1));
                newId = db.insert(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        null, values);
                break;
            default :
                throw new UnsupportedOperationException("Can't insert on " +uri);
        }//inserts on the adequate collection
        //if(newId <= 0) throw new SQLException("Couldn't insert");
        return ContentUris.withAppendedId(HocusFocusDBContract.HabitEntry.CONTENT_URI, newId);
    }//insert

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int deletedEntries;
        switch (CONTENT_MATCHER.match(uri)) {
            case HABIT :
                deletedEntries = db.delete(HocusFocusDBContract.HabitEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case HABIT_WITH_ID :
                deletedEntries = db.delete(HocusFocusDBContract.HabitEntry.TABLE_NAME,
                        HocusFocusDBContract.HabitEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)});
                break;
            case SUBHABIT :
                deletedEntries = db.delete(HocusFocusDBContract.SubHabitEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case SUBHABIT_WITH_ID :
                deletedEntries = db.delete(HocusFocusDBContract.SubHabitEntry.TABLE_NAME,
                        HocusFocusDBContract.HabitEntry._ID +" = ? AND "+HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT+" = ? ",
                        new String[] {uri.getPathSegments().get(1), uri.getPathSegments().get(2)});
                break;
            case NOTE:
                deletedEntries = db.delete(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case NOTE_WITH_ID:
                deletedEntries = db.delete(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        HocusFocusDBContract.HabitEntry._ID + " = ?",
                        new String[] {uri.getPathSegments().get(1)});
            default :
                throw new UnsupportedOperationException("Can't delete on " +uri);
        }//deletes from the requested collection
        Context context = getContext();
        if(context != null && deletedEntries > 0)
            context.getContentResolver().notifyChange(uri, null);
        return deletedEntries;
    }//delete

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int updatedEntries;
        switch (CONTENT_MATCHER.match(uri)) {
            case HABIT :
                updatedEntries = db.update(HocusFocusDBContract.HabitEntry.TABLE_NAME,
                        values, selection, selectionArgs);
                break;
            case HABIT_WITH_ID :
                updatedEntries = db.update(HocusFocusDBContract.HabitEntry.TABLE_NAME,
                        values, HocusFocusDBContract.HabitEntry._ID +" = ?",
                        new String[] {uri.getPathSegments().get(1)});
                break;
            case SUBHABIT :
                updatedEntries = db.update(HocusFocusDBContract.HabitEntry.TABLE_NAME,
                        values, selection, selectionArgs);
                break;
            case SUBHABIT_WITH_ID :
                updatedEntries = db.update(HocusFocusDBContract.SubHabitEntry.TABLE_NAME,
                        values, HocusFocusDBContract.SubHabitEntry._ID +" = ? AND "+HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT+" = ?",
                        new String[] {uri.getPathSegments().get(1), uri.getPathSegments().get(2)});
                break;
            case NOTE:
                updatedEntries = db.update(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        values, selection, selectionArgs);
                break;
            case NOTE_WITH_ID:
                updatedEntries = db.update(HocusFocusDBContract.NoteEntry.TABLE_NAME,
                        values, HocusFocusDBContract.NoteEntry._ID + " = ?",
                        new String[] {uri.getPathSegments().get(1)});
            default :
                throw new UnsupportedOperationException("Can't update " +uri);
        }//updates a given
        Context context = getContext();
        if(context != null && updatedEntries > 0) context.getContentResolver().notifyChange(uri, null);
        return updatedEntries;
    }//update

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (CONTENT_MATCHER.match(uri)) {
            case HABIT :
            case HABIT_WITH_ID :
                return HocusFocusDBContract.HabitEntry.CONTENT_TYPE;
            case SUBHABIT :
            case SUBHABIT_WITH_ID :
                return HocusFocusDBContract.SubHabitEntry.CONTENT_TYPE;
            case NOTE:
            case NOTE_WITH_ID:
                return HocusFocusDBContract.NoteEntry.CONTENT_TYPE;
            default :
                throw new UnsupportedOperationException("Unknown type for " +uri);
        }//returns the content type for the given URI
    }//getType

}//List Provider



