package unam.fciencias.hocusfocus;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import unam.fciencias.hocusfocus.Models.Note;

public class NewNote extends AppCompatActivity{
    private Note NOTA;
    EditText noteTitle;
    EditText noteContents;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        noteTitle = (EditText) findViewById(R.id.edit_note_title_et);
        noteContents = (EditText) findViewById(R.id.edit_note_contents_et);
        if(NOTA == null){
            NOTA = new Note();
        }
    }

    public void cancelAddNote(View view){
        finish();
    }

    public void addNote(View view){

        String noteTitleString = noteTitle.getText().toString();
        NOTA.setTitle(noteTitleString);

        String noteContentsString = noteContents.getText().toString();
        NOTA.setContents(noteContentsString);
        NOTA.setDate(System.currentTimeMillis());
        Uri laYuri = NOTA.insert(this);
        int id = Integer.parseInt(laYuri.getPathSegments().get(1));
        NOTA.setId(id);
        finish();
    }
}
