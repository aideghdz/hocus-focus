package unam.fciencias.hocusfocus.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase ayudante que crea y mantiene la base de datos de la app.
 */
public class HocusFocusDbHelper extends SQLiteOpenHelper {

    //Versión de la base de datos.
    public static final byte DATABASE_VERSION = 1;

    //Nombre de la bd.
    public static final String DATABASE_NAME = "hocus-focus";

    //Un ID para el CursorLoader para que interactue con la bd.
    public static final byte LIST_LOADER_ID = 3;

    //Constructor que crea un DB Helper.
    public HocusFocusDbHelper(Context context) {
        super(context, DATABASE_NAME + ".db", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder createQuery = new StringBuilder();
        createQuery.append("CREATE TABLE IF NOT EXISTS ")
                .append(HocusFocusDBContract.HabitEntry.TABLE_NAME)
                .append(" (").append(HocusFocusDBContract.HabitEntry._ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(HocusFocusDBContract.HabitEntry.COLUMN_NAME)
                .append(" TEXT NOT NULL DEFAULT '', ")
                .append(HocusFocusDBContract.HabitEntry.COLUMN_DAY)
                .append(" INTEGER NOT NULL, ")
                .append(HocusFocusDBContract.HabitEntry.COLUMN_HOUR)
                .append(" INTEGER NOT NULL, ")
                .append(HocusFocusDBContract.HabitEntry.COLUMN_ALARM)
                .append(" INTEGER NOT NULL DEFAULT 0); ");
        db.execSQL(createQuery.toString());
        StringBuilder createQuery2 = new StringBuilder();
        createQuery2.append("CREATE TABLE IF NOT EXISTS ")
                .append(HocusFocusDBContract.SubHabitEntry.TABLE_NAME)
                .append(" (").append(HocusFocusDBContract.SubHabitEntry._ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(HocusFocusDBContract.SubHabitEntry.COLUMN_HABIT)
                .append(" INTEGER NOT NULL REFERENCES ")
                .append(HocusFocusDBContract.HabitEntry.TABLE_NAME)
                .append("(").append(HocusFocusDBContract.HabitEntry._ID)
                .append(") ON DELETE CASCADE, ")
                .append(HocusFocusDBContract.SubHabitEntry.COLUMN_NAME)
                .append(" TEXT NOT NULL, ")
                .append(HocusFocusDBContract.SubHabitEntry.COLUMN_LENGTH)
                .append(" INTEGER NOT NULL, ")
                .append(HocusFocusDBContract.SubHabitEntry.COLUMN_CHECKED)
                .append(" INTEGER NOT NULL DEFAULT 0);");
        db.execSQL(createQuery2.toString());
        StringBuilder createQuery3 = new StringBuilder();
        createQuery3.append("CREATE TABLE IF NOT EXISTS ")
                .append(HocusFocusDBContract.NoteEntry.TABLE_NAME)
                .append(" (").append(HocusFocusDBContract.NoteEntry._ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(HocusFocusDBContract.NoteEntry.COLUMN_TITLE)
                .append(" TEXT NOT NULL, ")
                .append(HocusFocusDBContract.NoteEntry.COLUMN_CONTENTS)
                .append(" TEXT NOT NULL, ")
                .append(HocusFocusDBContract.NoteEntry.COLUMN_DATE)
                .append(" INTEGER NOT NULL);");
        db.execSQL(createQuery3.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Al ser una versión unica no incluimos nada.
    }
}
