package unam.fciencias.hocusfocus.Models;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.net.Uri;

import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;

public class Note {
    private int id;
    private String title;
    private String contents;
    private long date;

    public Note(int id, String title, String contents, long date){
        this.id = id;
        this.title = title;
        this.contents = contents;
        this.date = date;
    }

    public Note(String title, String contents, long date){
        this.id = Integer.parseInt(null);
        this.title = title;
        this.contents = contents;
        this.date = date;
    }

    public Note(){}

    public int getId(){
        return id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getContents(){
        return this.contents;
    }

    public long getDate(){
        return this.date;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContents(String contents){
        this.contents = contents;
    }

    public void setDate(long date){
        this.date = date;
    }

    public void update(String title, String contents, long date, Context context){
        this.title = title;
        this.contents = contents;
        this.date = date;
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_TITLE, title);
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_CONTENTS, contents);
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_DATE, date);
        context.getContentResolver().insert(
                HocusFocusDBContract.NoteEntry.CONTENT_URI, values);
    }

    public void delete(Context context) {
        context.getContentResolver().
                delete(HocusFocusDBContract.NoteEntry.buildListSizeUri(this.id),
                        null, null);
    }

    public Uri insert(Context context) {
        ContentValues values = new ContentValues();
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_TITLE, title);
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_CONTENTS, contents);
        values.put(HocusFocusDBContract.NoteEntry.COLUMN_DATE, date);
        return context.getContentResolver()
                .insert(HocusFocusDBContract.NoteEntry.CONTENT_URI, values);
    }

    public String formattedDateNoteRecyclerView() {
        Date date = new Date(this.date);
        SimpleDateFormat format = new SimpleDateFormat("d MMM yyyy",
                new Locale("es", "MX"));
        format.setTimeZone(TimeZone.getDefault());
        String dateString = format.format(date);
        return dateString;
    }

    public String formattedDateEditNote(){
        Date date = new Date(this.date);
        SimpleDateFormat format = new SimpleDateFormat("d MMM yyyy, HH:mm",
                new Locale("es", "MX"));
        format.setTimeZone(TimeZone.getDefault());
        String dateString = format.format(date);
        return dateString;
    }
}
