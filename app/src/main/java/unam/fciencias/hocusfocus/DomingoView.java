package unam.fciencias.hocusfocus;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import unam.fciencias.hocusfocus.Adapters.HabitListAdapter;
import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.DB.HocusFocusDbHelper;
import unam.fciencias.hocusfocus.Loaders.HabitListLoader;
import unam.fciencias.hocusfocus.Models.Habit;

/**
 * Fragmento del día domingo. Muestra los hábitos registrados para el día domingo
 */
public class DomingoView extends Fragment implements HabitListAdapter.MasterListItemClickHandler,
        LoaderManager.LoaderCallbacks{

    //atributos
    /**
     * Referencia a la lista que contendrá a los hábitos extraídos de la BD
     */
    private HabitListAdapter listAdapter;

    //Variable para saber si la lista ha cambiado(al borrar)
    private boolean hasListChanged;

    /**
     * Contructor, inicializa el fragmento y todos sus componentes, así como la lista donde
     * irán los hábitos.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_domingo_view, container, false);
        RecyclerView habitList = view.findViewById(R.id.lista_infinita);
        habitList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        habitList.setLayoutManager(layoutManager);
        listAdapter = new HabitListAdapter(getResources(), this);
        habitList.setAdapter(listAdapter);
        LoaderManager.getInstance(this).initLoader(HocusFocusDbHelper.LIST_LOADER_ID,
                null, this);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
    }//onDestroy

    @Override
    public void onItemClicked(String entryText) {

    }//onItemClicked

    /**
     * Crea el loader apropiado para obtener datos de la BD
     */
    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        switch (i) {
            case HabitListLoader.MASTER_LIST_LOADER_ID :
                return new HabitListLoader(this.getContext());

            case HocusFocusDbHelper.LIST_LOADER_ID :
                return new CursorLoader(this.getContext(), HabitListLoader.buildListSizeEntryUri(0),
                        new String[] {HocusFocusDBContract.HabitEntry._ID},
                        null, null, null);
            default :
                throw new UnsupportedOperationException("Can't create a Loader with ID " + i);
        }//creates the appropriate loader
    }//onCreateLoader

    /**
     * Reacciona a la finalización del loader.
     */
    @Override
    public void onLoadFinished(@NonNull Loader loader, Object results) {
        if(results instanceof Cursor) {
            Cursor result = (Cursor) results;
            Bundle listLoaderParams = new Bundle();
            listLoaderParams = null;
            LoaderManager.getInstance(this).initLoader(HabitListLoader.MASTER_LIST_LOADER_ID,
                    listLoaderParams, this).forceLoad();
        } else {
            List<Habit> resultados = (List<Habit>) results;
            LinkedList<Habit> aux = new LinkedList<Habit>();
            for (Habit h : resultados){
                aux.add(h);
            }
            for (Habit h : aux){
                if(!getDays(h).contains(1)){
                    resultados.remove(h);
                }
            }
            listAdapter.setDataset(resultados);
        }//reacts to the finalization of a Loader
    }//onLoadFinished

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        // unused
    }//onLoaderReset

    /**
     * Devuelve una codificación de los días seleeccionados por el usuario
     * para ser almacenados en la BD
     */
    public LinkedList<Integer> getDays(Habit HABITO){
        LinkedList<Integer> dias =  new LinkedList<Integer>();
        int dia = HABITO.getDay();
        if(dia-1000000 >= 0){
            dias.add(1);
            dia-=1000000;
        }
        if(dia-100000 >= 0){
            dias.add(7);
            dia-=100000;
        }
        if(dia-10000>= 0){
            dias.add(6);
            dia-= 10000;
        }
        if(dia-1000>= 0){
            dias.add(5);
            dia-= 1000;
        }
        if(dia-100>= 0){
            dias.add(4);
            dia-=100;
        }
        if(dia-10>= 0){
            dias.add(3);
            dia-=10;
        }
        if(dia-1>= 0){
            dias.add(2);
            dia-=1;
        }
        return dias;
    }
}
