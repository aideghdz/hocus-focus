package unam.fciencias.hocusfocus.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import unam.fciencias.hocusfocus.Loaders.SubHabitLoader;
import unam.fciencias.hocusfocus.Models.Habit;
import unam.fciencias.hocusfocus.Models.SubHabit;
import unam.fciencias.hocusfocus.NewHabito;
import unam.fciencias.hocusfocus.R;

public class HabitListAdapter extends RecyclerView.Adapter<HabitListAdapter.HabitEntry> {

    private List<Habit> dataset;
    private List<SubHabit> subHabitsDataSet;
    private Context context;
    private final Resources RESOURCES;

    private final MasterListItemClickHandler CLICK_HANDLER;

    public HabitListAdapter(Resources res, MasterListItemClickHandler listClickHandler) {
        RESOURCES = res;
        CLICK_HANDLER = listClickHandler;
    }

    @NonNull
    @Override
    public HabitEntry onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View tv = (View) LayoutInflater.from(context)
                .inflate(R.layout.habit_single_view, viewGroup, false);
        SubHabitLoader subhabitLoader = new SubHabitLoader(context);
        subHabitsDataSet = subhabitLoader.loadInBackground();
        return new HabitEntry(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull HabitEntry habitEntry, int i) {
        habitEntry.habit_name.setText(dataset.get(i).getName());
        habitEntry.hour.setText(dataset.get(i).hourToSting());
        habitEntry.subhabits.setText(subhabits(dataset.get(i)));
    }

    private String subhabits(Habit habit) {
        String result = "";
        int id = habit.getId();
        for(SubHabit sh: subHabitsDataSet){
            if(sh.getHabit() == id){
                result += sh.getName()+" \n";

            }
        }
        return result;
    }


    @Override
    public int getItemCount() {
        return dataset != null ? dataset.size() : 0;
    }

    public void setDataset(List<Habit> dataset) {
        this.dataset = dataset;
        notifyDataSetChanged();
    }


    class HabitEntry extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView hour;
        private TextView habit_name;
        private CheckBox alarma;
        private FloatingActionButton delete_button;
        private  TextView subhabits;
        HabitEntry(View entry) {
            super(entry);
            hour =(TextView) entry.findViewById(R.id.habit_hour_text);
            habit_name =(TextView) entry.findViewById(R.id.habit_name_text);
            subhabits = (TextView) entry.findViewById(R.id.habits_list);
            delete_button = (FloatingActionButton) entry.findViewById(R.id.habit_delete);
            delete_button.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Habit habit = dataset.remove(position);
                    habit.delete(v.getContext());
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, dataset.size());
                }
            });
            entry.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            NewHabito.update =true;
            int position = getAdapterPosition();
            Habit habit = dataset.get(position);
            NewHabito.HABITO =habit;
            Intent myIntent = new Intent(v.getContext(),NewHabito.class);
            v.getContext().startActivity(myIntent);

        }
    }
    public interface MasterListItemClickHandler {

        /**
         * Starts the Detail view to display data from the selected Master List's entry.
         * @param entryText - The text displayed in the selected list's entry.
         * @since MasterListItem ClickHandler 1.0, march 2019
         */
        void onItemClicked(String entryText);

    }


}
