package unam.fciencias.hocusfocus;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

import unam.fciencias.hocusfocus.Adapters.NoteListAdapter;
import unam.fciencias.hocusfocus.DB.HocusFocusDBContract;
import unam.fciencias.hocusfocus.DB.HocusFocusDbHelper;
import unam.fciencias.hocusfocus.Loaders.HabitListLoader;
import unam.fciencias.hocusfocus.Loaders.NoteListLoader;
import unam.fciencias.hocusfocus.Models.Note;

public class NoteListView extends Fragment
        implements NoteListAdapter.MasterListItemClickHandler,
        LoaderManager.LoaderCallbacks {

    private NoteListAdapter noteListAdapter;

    private boolean hasListChanged;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note_list_view, container, false);
        RecyclerView noteList = view.findViewById(R.id.lista_infinita);
        noteList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        noteList.setLayoutManager(layoutManager);
        noteListAdapter = new NoteListAdapter(getResources(), this);
        noteList.setAdapter(noteListAdapter);
        LoaderManager.getInstance(this).initLoader(HocusFocusDbHelper.LIST_LOADER_ID, null,
                this);
        return view;
    }

    @Override
    public void onItemClicked(String entryText) {

    }

    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
        switch(i) {
            case NoteListLoader.MASTER_LIST_LOADER_ID:
                return new NoteListLoader(this.getContext());
            case HocusFocusDbHelper.LIST_LOADER_ID:
                return new CursorLoader(this.getContext(),
                        HabitListLoader.buildListSizeEntryUri(0),
                        new String[] {HocusFocusDBContract.NoteEntry._ID},
                        null, null, null);
                default:
                    throw new UnsupportedOperationException("Can't create Loader with ID " + i);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object results) {
        if(results instanceof Cursor) {
            Cursor result = (Cursor) results;
            Bundle listLoaderParams = new Bundle();
            listLoaderParams = null;
            LoaderManager.getInstance(this).initLoader(NoteListLoader.MASTER_LIST_LOADER_ID,
                    listLoaderParams, this).forceLoad();
        } else {
            noteListAdapter.setDataset((List<Note>) results);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        //No se usa, OwO.
    }
}
